﻿namespace Base2art.Cryptography
{
    public interface ICryptionResult
    {
        bool IsSuccessful { get; }

        string AsText();

        byte[] AsBinary();
    }
}