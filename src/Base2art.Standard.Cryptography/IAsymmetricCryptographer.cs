﻿namespace Base2art.Cryptography
{
    public interface IAsymmetricCryptographer
    {
        IKeyExporter PublicKeyExporter { get; }
        IKeyExporter PrivateKeyExporter { get; }

        ICryptionResult Encrypt(string value);
        ICryptionResult Encrypt(byte[] value);

        ICryptionResult Decrypt(byte[] value);
    }
}