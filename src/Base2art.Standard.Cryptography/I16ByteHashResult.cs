﻿namespace Base2art.Cryptography
{
    using System;

    public interface I16ByteCryptionResult : ICryptionResult
    {
        Guid AsGuid();
    }
}