﻿namespace Base2art.Cryptography
{
    using System.IO;

    public interface IKeyExporter
    {
        void ExportKey(TextWriter outputStream);

        string ExportKey();
    }

    public interface IKeyExporter<in T>
    {
        void ExportKey(T csp, TextWriter outputStream);

        string ExportKey(T csp);
    }
}