﻿namespace Base2art.Cryptography.Internals
{
    using System.Globalization;
    using System.Text;

    internal class HashResult : ICryptionResult
    {
        public HashResult(byte[] hashedBytes)
        {
            this.HashedBytes = hashedBytes;
        }

        protected byte[] HashedBytes { get; }

        public bool IsSuccessful => this.HashedBytes != null;

        public byte[] AsBinary()
        {
            return this.HashedBytes;
        }

        public string AsText()
        {
            var hash = this.HashedBytes;

            // step 2, convert byte array to hex string
            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2", CultureInfo.InvariantCulture));
            }

            return sb.ToString();
        }
    }
}