﻿namespace Base2art.Cryptography.Internals.KeyManagement
{
    using System;
    using System.IO;
    using System.Security.Cryptography;

    internal class RsaPublicKeyExporter : KeyExporterBase<RSACryptoServiceProvider>
    {
        protected override void ExportKey(RSACryptoServiceProvider csp, TextWriter outputStream)
        {
            var parameters = csp.ExportParameters(false);
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write((byte) 0x30); // SEQUENCE
                using (var innerStream = new MemoryStream())
                {
                    var innerWriter = new BinaryWriter(innerStream);
                    innerWriter.Write((byte) 0x30); // SEQUENCE
                    innerWriter.EncodeLength(13);
                    innerWriter.Write((byte) 0x06); // OBJECT IDENTIFIER
                    var rsaEncryptionOid = new byte[]
                                           {
                                               0x2a,
                                               0x86,
                                               0x48,
                                               0x86,
                                               0xf7,
                                               0x0d,
                                               0x01,
                                               0x01,
                                               0x01
                                           };

                    innerWriter.EncodeLength(rsaEncryptionOid.Length);
                    innerWriter.Write(rsaEncryptionOid);
                    innerWriter.Write((byte) 0x05); // NULL
                    innerWriter.EncodeLength(0);
                    innerWriter.Write((byte) 0x03); // BIT STRING
                    using (var bitStringStream = new MemoryStream())
                    {
                        var bitStringWriter = new BinaryWriter(bitStringStream);
                        bitStringWriter.Write((byte) 0x00); // # of unused bits
                        bitStringWriter.Write((byte) 0x30); // SEQUENCE
                        using (var paramsStream = new MemoryStream())
                        {
                            var paramsWriter = new BinaryWriter(paramsStream);
                            paramsWriter.EncodeIntegerBigEndian(parameters.Modulus); // Modulus
                            paramsWriter.EncodeIntegerBigEndian(parameters.Exponent); // Exponent
                            var paramsLength = (int) paramsStream.Length;
                            bitStringWriter.EncodeLength(paramsLength);
                            bitStringWriter.Write(paramsStream.GetBuffer(), 0, paramsLength);
                        }

                        var bitStringLength = (int) bitStringStream.Length;
                        innerWriter.EncodeLength(bitStringLength);
                        innerWriter.Write(bitStringStream.GetBuffer(), 0, bitStringLength);
                    }

                    var length = (int) innerStream.Length;
                    writer.EncodeLength(length);
                    writer.Write(innerStream.GetBuffer(), 0, length);
                }

                var base64 = Convert.ToBase64String(stream.GetBuffer(), 0, (int) stream.Length).ToCharArray();
                outputStream.WriteLine("-----BEGIN PUBLIC KEY-----");
                for (var i = 0; i < base64.Length; i += 64)
                {
                    outputStream.WriteLine(base64, i, Math.Min(64, base64.Length - i));
                }

                outputStream.WriteLine("-----END PUBLIC KEY-----");
            }
        }
    }
}