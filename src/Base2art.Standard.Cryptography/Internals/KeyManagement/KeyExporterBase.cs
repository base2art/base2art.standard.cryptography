﻿namespace Base2art.Cryptography.Internals.KeyManagement
{
    using System;
    using System.IO;

    internal abstract class KeyExporterBase<T> : IKeyExporter<T>
    {
        void IKeyExporter<T>.ExportKey(T csp, TextWriter outputStream)
        {
            if (csp == null)
            {
                throw new ArgumentNullException(nameof(csp));
            }

            if (outputStream == null)
            {
                throw new ArgumentNullException(nameof(outputStream));
            }

            this.ExportKey(csp, outputStream);
        }

        string IKeyExporter<T>.ExportKey(T csp)
        {
            using (var writer = new StringWriter())
            {
                this.ExportKey(csp, writer);
                writer.Flush();
                return writer.GetStringBuilder().ToString();
            }
        }

        protected abstract void ExportKey(T csp, TextWriter outputStream);
    }
}