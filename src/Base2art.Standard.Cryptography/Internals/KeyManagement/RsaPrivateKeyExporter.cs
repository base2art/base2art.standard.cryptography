﻿namespace Base2art.Cryptography.Internals.KeyManagement
{
    using System;
    using System.IO;
    using System.Security.Cryptography;

    internal class RsaPrivateKeyExporter : KeyExporterBase<RSACryptoServiceProvider>
    {
        protected override void ExportKey(RSACryptoServiceProvider csp, TextWriter outputStream)
        {
            if (csp.PublicOnly)
            {
                throw new ArgumentException("CSP does not contain a private key", nameof(csp));
            }

            var parameters = csp.ExportParameters(true);
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write((byte) 0x30); // SEQUENCE
                using (var innerStream = new MemoryStream())
                {
                    var innerWriter = new BinaryWriter(innerStream);
                    innerWriter.EncodeIntegerBigEndian(new byte[] {0x00}); // Version
                    innerWriter.EncodeIntegerBigEndian(parameters.Modulus);
                    innerWriter.EncodeIntegerBigEndian(parameters.Exponent);
                    innerWriter.EncodeIntegerBigEndian(parameters.D);
                    innerWriter.EncodeIntegerBigEndian(parameters.P);
                    innerWriter.EncodeIntegerBigEndian(parameters.Q);
                    innerWriter.EncodeIntegerBigEndian(parameters.DP);
                    innerWriter.EncodeIntegerBigEndian(parameters.DQ);
                    innerWriter.EncodeIntegerBigEndian(parameters.InverseQ);
                    var length = (int) innerStream.Length;
                    writer.EncodeLength(length);
                    writer.Write(innerStream.GetBuffer(), 0, length);
                }

                var base64 = Convert.ToBase64String(stream.GetBuffer(), 0, (int) stream.Length).ToCharArray();
                outputStream.WriteLine("-----BEGIN RSA PRIVATE KEY-----");
                // Output as Base64 with lines chopped at 64 characters
                for (var i = 0; i < base64.Length; i += 64)
                {
                    outputStream.WriteLine(base64, i, Math.Min(64, base64.Length - i));
                }

                outputStream.WriteLine("-----END RSA PRIVATE KEY-----");
            }
        }
    }
}