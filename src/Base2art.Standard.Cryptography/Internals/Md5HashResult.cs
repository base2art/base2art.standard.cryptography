﻿namespace Base2art.Cryptography.Internals
{
    using System;

    internal class Md5HashResult : HashResult, I16ByteCryptionResult
    {
        public Md5HashResult(byte[] hashedBytes)
            : base(hashedBytes)
        {
        }

        public Guid AsGuid()
        {
            var hashedBytes = this.HashedBytes;
            var bytes = new byte[16];
            bytes[0] = hashedBytes[3];
            bytes[1] = hashedBytes[2];
            bytes[2] = hashedBytes[1];
            bytes[3] = hashedBytes[0];
            bytes[4] = hashedBytes[5];
            bytes[5] = hashedBytes[4];
            bytes[6] = hashedBytes[7];
            bytes[7] = hashedBytes[6];
            bytes[8] = hashedBytes[8];
            bytes[9] = hashedBytes[9];
            bytes[10] = hashedBytes[10];
            bytes[11] = hashedBytes[11];
            bytes[12] = hashedBytes[12];
            bytes[13] = hashedBytes[13];
            bytes[14] = hashedBytes[14];
            bytes[15] = hashedBytes[15];
            return new Guid(bytes);
        }
    }
}