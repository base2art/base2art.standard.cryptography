﻿namespace Base2art.Cryptography.Internals
{
    using System;

    internal class EncryptionResult : ICryptionResult
    {
        private readonly byte[] bytes;
        private readonly Func<byte[], string> converter;

        public EncryptionResult(byte[] bytes, Func<byte[], string> converter)
        {
            this.bytes = bytes;
            this.converter = converter;
        }

        public bool IsSuccessful => this.bytes != null;

        public string AsText()
        {
            return this.converter(this.bytes);
        }

        public byte[] AsBinary()
        {
            return this.bytes;
        }
    }
}