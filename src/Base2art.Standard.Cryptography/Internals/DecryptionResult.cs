﻿namespace Base2art.Cryptography.Internals
{
    using System;

    internal class DecryptionResult : ICryptionResult
    {
        private readonly StringResultValue stringWrapper;

        public DecryptionResult(byte[] bytes)
        {
            this.stringWrapper = new StringResultValue(bytes);
        }

        public bool IsSuccessful => this.stringWrapper.HasContent;

        public string AsText()
        {
            return !this.IsSuccessful
                       ? throw new InvalidOperationException("No Value for bytes")
                       : this.stringWrapper.AsString();
        }

        public byte[] AsBinary()
        {
            return !this.IsSuccessful
                       ? throw new InvalidOperationException("No Value for bytes")
                       : this.stringWrapper.AsByteArray();
        }
    }
}