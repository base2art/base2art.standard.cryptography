﻿namespace Base2art.Cryptography.Internals
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using KeyManagement;

    internal class RsaAsymmetricCryptographer : IAsymmetricCryptographer
    {
        private readonly RSAParameters rsaKeyInfo;

        public RsaAsymmetricCryptographer(RSAParameters rsaKeyInfo)
        {
            this.rsaKeyInfo = rsaKeyInfo;
            this.PublicKeyExporter = new KeyEporter(this.rsaKeyInfo, new RsaPublicKeyExporter());
            this.PrivateKeyExporter = new KeyEporter(this.rsaKeyInfo, new RsaPrivateKeyExporter());
        }

        public IKeyExporter PublicKeyExporter { get; }
        public IKeyExporter PrivateKeyExporter { get; }

        public ICryptionResult Encrypt(string value)
        {
            return this.Encrypt(Encoding.Default.GetBytes(value));
        }

        public ICryptionResult Encrypt(byte[] dataToEncrypt)
        {
            //Create a new instance of RSACryptoServiceProvider.
            using (var rsa = new RSACryptoServiceProvider())
            {
                //Import the RSA Key information. This only needs
                //toinclude the public key information.
                rsa.ImportParameters(this.rsaKeyInfo);

                //Encrypt the passed byte array and specify OAEP padding.  
                //OAEP padding is only available on Microsoft Windows XP or
                //later.  
                var encryptedData = rsa.Encrypt(dataToEncrypt, false);
                return new EncryptionResult(encryptedData, Convert.ToBase64String);
            }
        }

        public ICryptionResult Decrypt(byte[] dataToDecrypt)
        {
            //Create a new instance of RSACryptoServiceProvider.
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportParameters(this.rsaKeyInfo);

                var decryptedData = rsa.Decrypt(dataToDecrypt, false);
                return new DecryptionResult(decryptedData);
            }
        }

        private class KeyEporter : IKeyExporter
        {
            private readonly IKeyExporter<RSACryptoServiceProvider> keyExporter;
            private readonly RSAParameters rsaKeyInfo;

            public KeyEporter(RSAParameters rsaKeyInfo, IKeyExporter<RSACryptoServiceProvider> keyExporter)
            {
                this.rsaKeyInfo = rsaKeyInfo;
                this.keyExporter = keyExporter;
            }

            public void ExportKey(TextWriter outputStream)
            {
                using (var item = new RSACryptoServiceProvider())
                {
                    item.ImportParameters(this.rsaKeyInfo);
                    this.keyExporter.ExportKey(item, outputStream);
                }
            }

            public string ExportKey()
            {
                using (var item = new RSACryptoServiceProvider())
                {
                    item.ImportParameters(this.rsaKeyInfo);
                    return this.keyExporter.ExportKey(item);
                }
            }
        }
    }
}