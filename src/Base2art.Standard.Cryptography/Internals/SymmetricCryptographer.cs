﻿namespace Base2art.Cryptography.Internals
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using IO;

    internal class SymmetricCryptographer
    {
        private readonly Encoding encoder;

        public SymmetricCryptographer() : this(Encoding.Default)
        {
        }

        public SymmetricCryptographer(Encoding encoder)
        {
            this.encoder = encoder;
        }

        /// <summary>
        ///     Encrypt the given string using AES.  The string can be decrypted using
        ///     DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        /// <param name="salt">the items salt</param>
        public byte[] Encrypt<T>(string plainText, string sharedSecret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            return this.Encrypt<T>(sharedSecret, salt, csEncrypt =>
            {
                using (var swEncrypt = new StreamWriter(csEncrypt))
                {
                    swEncrypt.Write(plainText);
                }
            });
        }

        /// <summary>
        ///     Encrypt the given string using AES.  The string can be decrypted using
        ///     DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        /// <param name="salt">the items salt</param>
        public byte[] Encrypt<T>(byte[] plainText, string sharedSecret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            return this.Encrypt<T>(sharedSecret, salt, csEncrypt => csEncrypt.Write(plainText, 0, plainText.Length));
        }

        /// <summary>
        ///     Encrypt the given string using AES.  The string can be decrypted using
        ///     DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        /// <param name="salt">the items salt</param>
        /// <param name="writer">The writer.</param>
        private byte[] Encrypt<T>(string sharedSecret, string salt, Action<Stream> writer)
            where T : SymmetricAlgorithm, new()
        {
            if (string.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException(nameof(sharedSecret));
            }

            if (string.IsNullOrWhiteSpace(salt) || salt.Length < 8)
            {
                throw new ArgumentNullException(nameof(salt));
            }

            var saltBytes = this.encoder.GetBytes(salt);

            using (var aesAlg = new T())
            {
                try
                {
                    using (var key = new Rfc2898DeriveBytes(sharedSecret, saltBytes))
                    {
                        aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    }

                    using (var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV))
                    {
                        using (var msEncrypt = new MemoryStream())
                        {
                            msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                            msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                            using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                            {
                                writer(csEncrypt);
                            }

                            return msEncrypt.ToArray();
                        }
                    }
                }
                finally
                {
                    // Clear the RijndaelManaged object.
                    aesAlg.Clear();
                }
            }
        }

        /// <summary>
        ///     Decrypt the given string.  Assumes the string was encrypted using
        ///     EncryptStringAES(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        /// <param name="salt">The salt</param>
        public byte[] Decrypt<T>(byte[] cipherText, string sharedSecret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            if (string.IsNullOrWhiteSpace(salt) || salt.Length < 8)
            {
                throw new ArgumentNullException(nameof(salt));
            }

            if (cipherText == null)
            {
                throw new ArgumentNullException(nameof(cipherText));
            }

            if (string.IsNullOrWhiteSpace(sharedSecret))
            {
                throw new ArgumentNullException("Value cannot be null or whitespace.", nameof(sharedSecret));
            }

            var saltBytes = this.encoder.GetBytes(salt);

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            using (var aesAlg = new T())
            {
                try
                {
                    // generate the key from the shared secret and the salt
                    using (var key = new Rfc2898DeriveBytes(sharedSecret, saltBytes))
                    {
                        using (var msDecrypt = new MemoryStream(cipherText))
                        {
                            msDecrypt.Seek(0, SeekOrigin.Begin);

                            // Create a RijndaelManaged object
                            // with the specified key and IV.
                            aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                            // Get the initialization vector from the encrypted stream
                            var buf = new byte[4];
                            msDecrypt.Read(buf, 0, sizeof(int));
                            var ivLength = BitConverter.ToInt32(buf, 0);

                            buf = new byte[ivLength];

                            msDecrypt.Read(buf, 0, ivLength);

                            aesAlg.IV = buf;

                            // Create a decrytor to perform the stream transform.
                            using (var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV))
                            {
                                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                                {
                                    return csDecrypt.ReadFully();
                                }
                            }
                        }
                    }
                }
                finally
                {
                    // Clear the RijndaelManaged object.
                    aesAlg.Clear();
                }
            }
        }
    }
}