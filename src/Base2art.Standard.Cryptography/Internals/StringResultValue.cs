﻿namespace Base2art.Cryptography.Internals
{
    using System;
    using System.IO;

    internal class StringResultValue // : IHashResult
    {
        private readonly byte[] bytes;
        private readonly Lazy<string> value;

        public StringResultValue(byte[] bytes)
        {
            this.bytes = bytes;
            this.value = new Lazy<string>(this.Create);
        }

        public bool HasContent => this.bytes != null;

        public string AsString()
        {
            return this.value.Value;
        }

        public byte[] AsByteArray()
        {
            return this.bytes;
        }

        private string Create()
        {
            using (var ms = new MemoryStream(this.bytes))
            {
                ms.Seek(0L, SeekOrigin.Begin);

                using (var sr = new StreamReader(ms))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }
}