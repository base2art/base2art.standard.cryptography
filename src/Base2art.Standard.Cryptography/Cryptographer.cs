﻿namespace Base2art.Cryptography
{
    using System;
    using System.Security.Cryptography;
    using System.Text;
    using Internals;

    public static class Cryptographer
    {
        private static readonly SymmetricCryptographer Symmetry = new SymmetricCryptographer();

        public static ICryptionResult Encrypt<T>(this string value, string secret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            return new EncryptionResult(Symmetry.Encrypt<T>(value ?? string.Empty, secret, salt), Convert.ToBase64String);
        }

        public static ICryptionResult Encrypt<T>(this byte[] value, string secret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            return new EncryptionResult(Symmetry.Encrypt<T>(value, secret, salt), Convert.ToBase64String);
        }

        public static ICryptionResult Decrypt<T>(this string value, string secret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            return Convert.FromBase64String(value).Decrypt<T>(secret, salt);
        }

        public static ICryptionResult Decrypt<T>(this byte[] value, string secret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            try
            {
                var item = Symmetry.Decrypt<T>(value, secret, salt);
                return new DecryptionResult(item);
            }
            catch (CryptographicException)
            {
                return new DecryptionResult(null);
            }
        }

        public static IAsymmetricCryptographer CreateRsa(RSAParameters parameters)
        {
            return new RsaAsymmetricCryptographer(parameters);
        }

        public static RSAParameters CreateRsaParameters()
        {
            var myRsa = new RSACryptoServiceProvider(2048);

            var key = myRsa.ExportParameters(true);

            return key;
        }

        public static ICryptionResult Hash<T>(this string value)
            where T : HashAlgorithm, new()
        {
            return CryptionResult<T, HashResult>(value, x => new HashResult(x));
        }

        public static I16ByteCryptionResult Hash(this string value)
        {
            return CryptionResult<MD5CryptoServiceProvider, Md5HashResult>(value, x => new Md5HashResult(x));
        }

        private static TResult CryptionResult<T, TResult>(string value, Func<byte[], TResult> creator)
            where T : HashAlgorithm, new()
            where TResult : ICryptionResult
        {
            var inputBytes = Encoding.Default.GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = new T())
            {
                return creator(hashAlgorithm.ComputeHash(inputBytes));
            }
        }
    }
}