﻿namespace Base2art.Cryptography
{
    using System;
    using System.Collections.Generic;
    using FluentAssertions;
    using Xunit;

    public class GuidHashingFeature
    {
        private void VerifyHash(string value, HashSet<string> theSet)
        {
            var hashResult = value.Hash();
            var hashedValue = hashResult.AsText();
            if (theSet.Contains(hashedValue))
            {
                throw new Exception();
            }

            theSet.Add(hashedValue);
            hashResult.AsGuid().Should().Be(new Guid(hashedValue));
        }

        [Fact]
        public void ShouldHashCorrecty()
        {
            "base2art".Hash().AsText().Should().Be("47AEDA1582AE5E4838A1452F290A7F27");
            var set = new HashSet<string>();
            this.VerifyHash("base2art", set);
            this.VerifyHash("base2Art", set);
            for (int i = 'A'; i <= 'Z'; i++)
            {
                this.VerifyHash(new string((char) i, 1), set);
                this.VerifyHash(new string((char) i, 2), set);
                this.VerifyHash(new string((char) i, 3), set);
                this.VerifyHash(new string((char) i, 4), set);
            }

            for (int i = 'a'; i <= 'z'; i++)
            {
                this.VerifyHash(new string((char) i, 1), set);
                this.VerifyHash(new string((char) i, 2), set);
                this.VerifyHash(new string((char) i, 3), set);
                this.VerifyHash(new string((char) i, 4), set);
            }
        }
    }
}