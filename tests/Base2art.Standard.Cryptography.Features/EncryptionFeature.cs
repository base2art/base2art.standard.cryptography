﻿namespace Base2art.Cryptography
{
    using System;
    using System.Security.Cryptography;
    using System.Text;
    using FluentAssertions;
    using Xunit;

    public class EncryptionFeature
    {
        [Fact]
        public async void ShouldEncryptAndDecryptAssymmetric()
        {
            var content = "Hello World!";
            var salt = new Guid("7C909A47C1874FFBAA73BAE132C2C9D9").ToString("N");
            var secret = "This is a real Secret";

            var item1 = content.Encrypt<AesManaged>(secret, salt).AsText();
            var item2 = content.Encrypt<AesManaged>(secret, salt).AsText();

            item1.Should().NotBe(item2);

            var decryptionResult1 = item1.Decrypt<AesManaged>(secret, salt);
            var decryptionResult2 = item2.Decrypt<AesManaged>(secret, salt);

            decryptionResult1.IsSuccessful.Should().BeTrue();

            decryptionResult1.AsText().Should().Be(content);
            decryptionResult2.AsText().Should().Be(content);
        }

        [Fact]
        public async void ShouldEncryptAndDecryptSymmetric()
        {
            var content = "Hello World!";
            var salt = new Guid("7C909A47C1874FFBAA73BAE132C2C9D9").ToString("N");
            var secret = "This is a real Secret";

            var item1 = content.Encrypt<AesManaged>(secret, salt).AsText();
            var item2 = content.Encrypt<AesManaged>(secret, salt).AsText();

            item1.Should().NotBe(item2);

            var decryptionResult1 = item1.Decrypt<AesManaged>(secret, salt);
            var decryptionResult2 = item2.Decrypt<AesManaged>(secret, salt);

            decryptionResult1.IsSuccessful.Should().BeTrue();

            decryptionResult1.AsText().Should().Be(content);
            decryptionResult2.AsText().Should().Be(content);
        }

        [Fact]
        public async void ShouldEncryptAndDecryptSymmetricByteWiseApi()
        {
            var contentText = "Hello World!";
            var content = Encoding.Default.GetBytes(contentText);
            var salt = new Guid("7C909A47C1874FFBAA73BAE132C2C9D9").ToString("N");
            var secret = "This is a real Secret";

            var item1 = content.Encrypt<AesManaged>(secret, salt).AsText();
            var item2 = content.Encrypt<AesManaged>(secret, salt).AsText();

            item1.Should().NotBe(item2);

            var decryptionResult1 = item1.Decrypt<AesManaged>(secret, salt);
            var decryptionResult2 = item2.Decrypt<AesManaged>(secret, salt);

            decryptionResult1.IsSuccessful.Should().BeTrue();

            decryptionResult1.AsText().Should().Be(contentText);
            decryptionResult2.AsText().Should().Be(contentText);
        }

        [Fact]
        public async void ShouldEncryptAndDecryptSymmetricByteWiseApi2()
        {
            var contentText = "Hello World!";
            var content = Encoding.Default.GetBytes(contentText);
            var salt = new Guid("7C909A47C1874FFBAA73BAE132C2C9D9").ToString("N");
            var secret = "This is a real Secret";

            var item1 = content.Encrypt<AesManaged>(secret, salt).AsBinary();
            var item2 = content.Encrypt<AesManaged>(secret, salt).AsBinary();

            item1.Should().NotBeEquivalentTo(item2);

            var decryptionResult1 = item1.Decrypt<AesManaged>(secret, salt);
            var decryptionResult2 = item2.Decrypt<AesManaged>(secret, salt);

            decryptionResult1.IsSuccessful.Should().BeTrue();

            decryptionResult1.AsText().Should().Be(contentText);
            decryptionResult2.AsText().Should().Be(contentText);
        }
    }
}