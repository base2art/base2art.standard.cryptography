﻿namespace Base2art.Cryptography
{
    using System.IO;
    using System.Reflection;
    using System.Security.Cryptography;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Xunit;

    public class AsyncEncryptionFeature
    {
        [Fact]
        public async void ShouldEncryptAndDecryptAsymmetric()
        {
            var content = "Hello World!";
            var secret = "This is a real Secret";

            var parameters = Cryptographer.CreateRsaParameters();

            var cryptographer = Cryptographer.CreateRsa(parameters);

            var item1 = cryptographer.Encrypt(content).AsBinary();
            var item2 = cryptographer.Encrypt(content).AsBinary();

            item1.Should().NotBeEquivalentTo(item2);

            var decryptionResult1 = cryptographer.Decrypt(item1);
            var decryptionResult2 = cryptographer.Decrypt(item2);

            decryptionResult1.IsSuccessful.Should().BeTrue();

            decryptionResult1.AsText().Should().Be(content);
            decryptionResult2.AsText().Should().Be(content);
        }

        [Fact]
        public async void ShouldLoadKeyAndWriteItToDisk()
        {
            var asm = Assembly.GetExecutingAssembly();

            using (var stream = asm.GetManifestResourceStream("Base2art.Cryptography.Content.json"))
            {
                using (var reader = new StreamReader(stream))
                {
                    var content = reader.ReadToEnd();

                    var output = JsonConvert.DeserializeObject<RSAParameters>(content);

                    var t = Cryptographer.CreateRsa(output).PrivateKeyExporter.ExportKey();
                    t.Trim().Should().Be(@"
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA9h/X74UNOAX8RvompeEpOvufw+0hCPK0ggJz8l2PRRNPaWgS
YdCoP6SeUSes3TSH0PJz43mXVRlzP0ZngEW3lnoD/rIzucAC6+lMl5ZOx3bwOzaf
hAR9Ussu1v3CzaRNBbzHHj2sLgSMAqxV8u2sMKfp6DlnqKGjzUWrv9n/lvUYu7ju
6NW5AQmF+JQ4DhNXmEJUNjKJPr0oHsVwynMx7/H+FFKCp7nogdGUxU9bar5R7mKE
+UDRKyzD55YdsoaYZWjjgPQVdOdj5tvcTLt73INAb/zxnj9cv1xw/kXBM71XuFre
vJASTnos8JUrHUXVXfBFlupyzf0J+OArT/lTgQIDAQABAoIBAB5QfcA6mw4zbocv
eGMmAHYLV/pB0rfPbPw2YSGQ9HyFjFpnPxxdHtv34qgMQvZb6f6KXku+Ri1tHQzG
FyltbNdT3/oW6NgQFkwISC7DjKB0vdINQEh6ZRU1gWdpifzaJ8J1JfFLl1FzresG
Xu+05mh3xg+cQnA6uHooAqd2qkxlVfcJAWVOK+bsH7mDO0XUUGPrWY7ksE6BdHcM
CQ/RfJlGFUNu/y3CBRTrmFqPzIlR2gH7/aIDz+KR59tKdA+KkPk6zzF6KIYK+PdX
pzhomiHf08WJLEPxy/qp+ZWhiDDiOjur1js0OSl6324gBx+SWU5sgSV3225h3XH1
wdCg2YECgYEA/vz432X+HCB/yIyD2Oc/WZstqBHODdFPoI4uwqC/0bnIBq6i79Hk
vKYyE4KQ7FP51e08p/+J95H3uXUbtUyuQdRuKUOWMtGZM9FrieSZSzQhisVscmxx
ZnCD1mwZVvKxp918LcCZqhHGXlDyN4Nowxah82rEyAblg0/tA6x24K0CgYEA9xnd
/Eg/WDZLHWblV4Gr9h2xCrURCOZlEAxi6y+1hOm1z4P/KGCT4EizIKc19onAkrei
MkmW0krAggldwiZ1oEcHJW0FyD65CK8r3TARXTu6lcgXq6+1m5b3bCdCjrZwkTjO
ZKVeCe1qM1FBW6M0UnG+/BmYEJLpe2AIF/WhFKUCgYBDE5kEa3glPgdfYldsv6hS
khe4lrZ1vBIOUljKqi7LpOuRlm8WV0ist6uEplwpTw3x0K6cIwTQM33PM5dOYhWV
C6TPBhCE3c5Ha1e2toP83IHWV4jnsM9D+2UzCsy/qqhBWGpKPtgtfO+J8BFHuTHd
4B5KQlPxqp7FpEjdJok/IQKBgH02HVduYLLZaDyw3Dv4WGPxbbnoQIaLAOEd9a4J
UejU6K0LzOrv7V70V9iPAng4G139MXJb+dH70QrTpi/wfyBFfaiIUyrCJc7xvL9r
URE2Z/joGJ6F/femix/B0Scx6V6dM3CqnzlWlb8FSKIf1C6GtulWKskTOL6szc0e
SY6FAoGBALfICyWuh/hZ4ec50zj0Fu+uFN/kLbBUgA3U8j/5Yl07sP1+jHFgEtlo
lN9vjLtTfxkylTI+9F3p7ejodMW8CTyfntFd/OVTCQFoX42/7BMLsYXKQX8UXChk
nxLsL6OnGN1oQtRJ5MUd7HbsGFTWZsKbMAaYrlYKOMlyjy0/BlJb
-----END RSA PRIVATE KEY-----
".Trim());

                    t = Cryptographer.CreateRsa(output).PublicKeyExporter.ExportKey();
                    t.Trim().Should().Be(@"
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9h/X74UNOAX8RvompeEp
Ovufw+0hCPK0ggJz8l2PRRNPaWgSYdCoP6SeUSes3TSH0PJz43mXVRlzP0ZngEW3
lnoD/rIzucAC6+lMl5ZOx3bwOzafhAR9Ussu1v3CzaRNBbzHHj2sLgSMAqxV8u2s
MKfp6DlnqKGjzUWrv9n/lvUYu7ju6NW5AQmF+JQ4DhNXmEJUNjKJPr0oHsVwynMx
7/H+FFKCp7nogdGUxU9bar5R7mKE+UDRKyzD55YdsoaYZWjjgPQVdOdj5tvcTLt7
3INAb/zxnj9cv1xw/kXBM71XuFrevJASTnos8JUrHUXVXfBFlupyzf0J+OArT/lT
gQIDAQAB
-----END PUBLIC KEY-----
".Trim());
                }
            }
        }
    }
}