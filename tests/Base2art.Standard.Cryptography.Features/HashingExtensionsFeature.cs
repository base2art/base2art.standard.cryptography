﻿namespace Base2art.Cryptography
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using FluentAssertions;
    using Xunit;

    public class HashingExtensionsFeature
    {
        [Theory]
        [InlineData("value", "secret", null, true)]
        [InlineData("value", "secret", "", true)]
        [InlineData("value", "secret", "salt", true)]
        [InlineData("value", "", "saltsalt", true)]
        [InlineData("value", null, "saltsalt", true)]
        [InlineData(null, "secret", "saltsalt", false)]
        [InlineData("", "secret", "saltsalt", false)]
        [InlineData("value", "secret", "saltsalt", false)]
        public void ShouldTestNull_Encrypt(string value, string secret, string salt, bool exception)
        {
            if (exception)
            {
                this.ExpectArgError(() => value.Encrypt<DESCryptoServiceProvider>(secret, salt));
            }
            else
            {
                this.ExpectNoError(() => value.Encrypt<DESCryptoServiceProvider>(secret, salt));
            }
        }

        [Theory]
        [InlineData("value", "secret", null, true)]
        [InlineData("value", "secret", "", true)]
        [InlineData("value", "secret", "salt", true)]
        [InlineData("value", "", "saltsalt", true)]
        [InlineData("value", null, "saltsalt", true)]
        [InlineData(null, "secret", "saltsalt", true)]
        [InlineData("", "secret", "saltsalt", true)]
        [InlineData("value", "secret", "saltsalt", false)]
        public void ShouldTestNull_Decrypt(string value, string secret, string salt, bool exception)
        {
            var encrypted = value.Encrypt<DESCryptoServiceProvider>("This is a test Secret", "SaltSalt")
                                 .AsText();

            if (string.IsNullOrWhiteSpace(value))
            {
                encrypted = value;
            }

            if (exception)
            {
                this.ExpectArgError(() => encrypted.Decrypt<DESCryptoServiceProvider>(secret, salt));
            }
            else
            {
                this.ExpectNoError(() => encrypted.Decrypt<DESCryptoServiceProvider>(secret, salt));
            }
        }

        [Theory]
        [InlineData("Base2art", "MY PERSONAL SECRET", "seaSalt!", "MY PERSONAL SECRET", "seaSalt!", true)]
        [InlineData("Base2art", "MY PERSONAL SECRET", "seaSalt!", "MY PERSONAL SECREt", "seaSalt!", false)]
        [InlineData("Base2art", "MY PERSONAL SECRET", "seaSalt!", "MY PERSONAL SECRET", "seaSalt*", false)]
        [InlineData("Base2art", "seaSalt!", "MY PERSONAL SECRET", "MY PERSONAL SECRET", "seaSalt!", false)]
        public void ShouldEncryptCorrecty(
            string input,
            string encryptSecret, string encryptSalt,
            string decryptSecret, string decryptSalt,
            bool shouldWork)
        {
            var encryptionResult = input.Encrypt<AesManaged>(encryptSecret, encryptSalt);
            var encryptedBytes = encryptionResult.AsBinary();

            var output = encryptedBytes.Decrypt<AesManaged>(decryptSecret, decryptSalt);
            output.IsSuccessful.Should().Be(shouldWork);

            if (shouldWork)
            {
                output.AsText().Should().Be(input);
            }

            var encryptedString = encryptionResult.AsText();
            var outputFromString = encryptedString.Decrypt<AesManaged>(decryptSecret, decryptSalt);
            outputFromString.IsSuccessful.Should().Be(shouldWork);

            if (shouldWork)
            {
                outputFromString.AsText().Should().Be(input);
            }
        }

        private void VerifyHash(string value, HashSet<string> theSet)
        {
            var hashResult = value.Hash<MD5CryptoServiceProvider>();
            var hashedValue = hashResult.AsText();

            if (theSet.Contains(hashedValue))
            {
                throw new Exception();
            }

            theSet.Add(hashedValue);

            hashResult.AsText().Should().Be(hashedValue);
        }

        private void ExpectArgError(Action action)
        {
            action.ShouldThrow<ArgumentNullException>();
        }

        private void ExpectNoError(Action action)
        {
            action.ShouldNotThrow();
        }

        [Fact]
        public void ShouldHashCorrectly_AsBinary()
        {
            Convert.ToBase64String("base2art".Hash<MD5CryptoServiceProvider>().AsBinary()).Should().Be("R67aFYKuXkg4oUUvKQp/Jw==");
        }

        [Fact]
        public void ShouldHashCorrecty()
        {
            "base2art".Hash<MD5CryptoServiceProvider>().AsText().Should().Be("47AEDA1582AE5E4838A1452F290A7F27");
            var set = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            this.VerifyHash("base2art", set);
            this.VerifyHash("base2Art", set);

            for (int i = 'A'; i <= 'Z'; i++)
            {
                this.VerifyHash(new string((char) i, 1), set);
                this.VerifyHash(new string((char) i, 2), set);
                this.VerifyHash(new string((char) i, 3), set);
                this.VerifyHash(new string((char) i, 4), set);
            }

            for (int i = 'a'; i <= 'z'; i++)
            {
                this.VerifyHash(new string((char) i, 1), set);
                this.VerifyHash(new string((char) i, 2), set);
                this.VerifyHash(new string((char) i, 3), set);
                this.VerifyHash(new string((char) i, 4), set);
            }
        }

        [Fact]
        public void ShouldTestNull_Decrypt_Bytes()
        {
            byte[] item = null;
            this.ExpectArgError(() => item.Decrypt<DESCryptoServiceProvider>("My Secret", "Salt Salt"));
        }
    }
}